from django.contrib import auth
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.tokens import RefreshToken, TokenError

from accounts.models import User
from utils import messages

"""
RegisterSerializer - сериалайзер отвечающий за регистрацию
Принимает при регистрации три поле, обязательные для создания объекта класса User
(пользователя) -  email, password, phone_number
"""


class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=68, min_length=6)

    class Meta:
        model = User
        fields = ['email', 'password', 'phone_number']


"""
Сериалазйер для входа пользователя в систему
Принимает поля почта или пароль, номер телефона или пароль. Если пользователь
ввел корректные данные для аутентификации (почту и пароль) которые смогут пройти
все этапы валидации, то сервер возвращает ему два токена access и refresh.
С помощью access токена он авторизуется в системе и может совершать действия
от своего лица. Но access токен имеет срок годности параметр которого задается в файлах
настроек и когда он истечет а сессия пользователя еще не закончится access токен будет
обновлен с помощью refresh токена и сессия пользователя продолжится.
"""


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255, min_length=3, required=False)
    phone_number = serializers.CharField(max_length=20, min_length=3, required=False)
    password = serializers.CharField(max_length=68, min_length=6, write_only=True)
    tokens = serializers.SerializerMethodField()

    def get_tokens(self, obj):  # noqa
        try:
            if obj.get('phone_number'):
                user = get_user_model().objects.get(phone_number=obj['phone_number'])
            elif obj.get('email'):
                user = get_user_model().objects.get(email=obj['email'])
            else:
                raise ValueError('No phone number or email provided')
        except User.DoesNotExist:
            raise AuthenticationFailed('Invalid credentials, try again')
        return {'access': user.tokens()['access'],
                'refresh': user.tokens()['refresh']}

    class Meta:
        model = User
        fields = ['email', 'password', 'tokens']

    def validate(self, attrs):  # noqa
        email = attrs.get('email', None)
        password = attrs.get('password', None)
        phone_number = attrs.get('phone_number', None)

        if email:
            user = auth.authenticate(email=email, password=password)
        elif phone_number:
            try:
                user_email = User.objects.get(phone_number=phone_number).email
            except User.DoesNotExist:
                raise AuthenticationFailed('Invalid credentials, try again')
            user = auth.authenticate(email=user_email, password=password)
        else:
            raise ValueError('No phone number or email provided')
        if not user:
            raise AuthenticationFailed('Invalid credentials, try again')
        if not user.is_active:
            raise AuthenticationFailed('Account disabled, contact admin')
        return super().validate(attrs)


"""
Сериалайзер выхода из системы.
Этот сериалайзер отвечает за окончание сессии пользователем. Когда пользователь хочет выйти из
системы и нажимает на кнопку выйти происходит следующее. Фронт стирает access токен а refresh
токен отправляется на этот сериалайзер который отправляет его в "Черный Список". На этапе
валидации сравнивается токен пользователя сохраненный сервером с токеном присланным с фронта.
Сессия заканчивается ибо у пользователя нет access токена и он не сможет его обновить с
помощью refresh токена. С этого момента он является неавторизованным.
"""


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField(error_messages={'blank': messages.ENTER_REFRESH_TOKEN})
    default_error_messages = {'bad_token': messages.TEXT_UNAUTHORIZED}

    def validate(self, attrs):
        self.token = attrs['refresh']
        return attrs

    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()
        except TokenError:
            self.fail('bad_token')


"""
Сериалайзер, просто возвращающий все необходимые поля из модели пользователя. Вся
информация чисто справочная
"""


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'email', 'phone_number']


"""
Сериалайзер для восстановления пароля пользователя.
Принимает почту, с которой был зарегистрирован аккаунт пароля.
Поскольку приложение может быть развернуто на нескольких разных серверах и получать
запросы от разных серверов (например песочница бэкенда получает запросы от demo frontend), то
важно присылать письма с релевантными адресами - именно поэтому есть поле redirect_url - в
этом поле фронт указывает на какой сервер должно вести письмо с токеном аутентификации. Этот
урл указывается в письме, которое отправляется пользователю.
"""


class RequestPasswordResetEmailSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255, min_length=3, required=True)
    redirect_url = serializers.CharField(max_length=500, required=False)

    class Meta:
        model = get_user_model()
        fields = ['email', 'redirect_url']


"""
Сериалайзер для проверки uid кода пользователя и токена для сброса пароля пользователя.
"""


class PasswordTokenCheckSerializer(serializers.ModelSerializer):
    uidb64 = serializers.CharField()
    token = serializers.CharField()

    class Meta:
        model = get_user_model()
        fields = ['uidb64', 'token']


"""
Сериалайзер для создания нового пароля.
В предыдущем сериалайзере мы приняли от пользователя запрос на восстановление пароля. После
того как пользователь вошел в свой почтовый ящик и перешел по ссылке для восстановления пароля,
он попадает на страницу где он вводит свой новый пароль. Проверка пароля на соответствие
всяким нормам безопасности проходит на стороне клиентского приложения. Мы же проверяем
является ли пользователь владельцем того самого почтового адреса, который был указан в
предыдущем сериалайзере. Для проверки в письме на восстановление пароля мы выслали токен
аутентификации и uid код. Если пользователь передает их нам и при валидации они проходят все
проверки на сервере то это значит что пользователь является владельцем того самого аккаунта к
которому привязана почта на которую была выслана ссылка для подтверждения пароля. После
прохождения проверки токенов пароль пользователя принимается и устанавливается в качестве
ключа к аккаунту с почтой.

Этот сериалайзер принимает пароль, токен и uid код.
"""


class SetNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(min_length=6, max_length=68, write_only=True, required=True)
    token = serializers.CharField(min_length=1, write_only=True, required=True)
    uidb64 = serializers.CharField(min_length=1, write_only=True, required=True)

    class Meta:
        fields = ['password', 'token', 'uidb64']

    def validate(self, attrs):  # noqa
        try:
            password = attrs.get('password')
            token = attrs.get('token')
            uidb64 = attrs.get('uidb64')

            user_id = force_str(urlsafe_base64_decode(uidb64))
            user = get_user_model().objects.get(id=user_id)
            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('Reset link is invalid', 401)

            user.set_password(password)
            user.save()
        except Exception:
            raise AuthenticationFailed('Reset link is invalid', 401)
        return super().validate(attrs)
