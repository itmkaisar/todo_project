from django.db import models

from accounts.models import User

""" Модель задачи (Task)
- ID (primary key) - int
- Владелец задачи - (пользователь создавший задачу)
- Заголовок задачи - char[100]
- Описание задачи - text
- Срок исполнения - datetime
- Выполнено - boolean
"""


class Task(models.Model):
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='tasks', verbose_name='Owner')
    title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Title')
    description = models.TextField(
        max_length=3000, null=False, blank=False, verbose_name='Description')
    execution_time = models.DateTimeField(null=False, blank=False, verbose_name='Execution Time')
    is_done = models.BooleanField(default=False, verbose_name='Is done')

    def __str__(self):
        return self.title
