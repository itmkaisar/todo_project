from rest_framework import serializers

from api.models import Task

"""
Сериализатор созданий задачи. Убрал is_done и owner.
Созданная задача не может быть выполненной. И авторизованный юзер становятся автором
"""


class TaskCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        exclude = ['is_done', 'owner']


"""
Сериализатор списка задачи. Выводятся поля ID, Заголовок, Срок исполнения, Выполнено
"""


class TaskListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'title', 'execution_time', 'is_done']


"""
Сериализатор изменение задачи. Выводятся все поля кроме автора
"""


class TaskUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        exclude = ['owner']


"""
Сериализатор где выводятся все поля
"""


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'


"""
Сериализатор ручка для пометки задачи выполненной 
"""


class TaskIsDoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['is_done']
