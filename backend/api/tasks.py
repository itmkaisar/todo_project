from api.models import Task
from todo.celery import app
from utils.email_service import SendEmail


@app.task(name='send_task_completion_email')
def send_task_completion_email(task_id, is_done):
    task = Task.objects.get(id=task_id)
    subject = 'Task Completed' if is_done else 'Task Unmarked'
    message = f'Your task {task.title} has been marked as {is_done}.'
    SendEmail.send_email(task.owner.email, subject, message)
