from django.urls import path, include
from rest_framework.routers import SimpleRouter

from api.views import TaskViewSet, TaskExecuteView

router = SimpleRouter()

router.register('todo', TaskViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('todo/<int:id>/execute/', TaskExecuteView.as_view()),
]
