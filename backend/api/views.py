from django.http import Http404, JsonResponse
from rest_framework import status, permissions
from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from api.models import Task
from api.serializers import (
    TaskCreateSerializer, TaskListSerializer, TaskUpdateSerializer,
    TaskSerializer, TaskIsDoneSerializer
)
from api.tasks import send_task_completion_email
from utils.messages import NO_TASK
from utils.permissions import IsOwner


class TaskViewSet(ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [permissions.IsAuthenticated, IsOwner]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(owner=self.request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_update(self, serializer):
        is_done = serializer.validated_data.get('is_done', getattr(serializer.instance, 'is_done'))
        serializer.save()
        send_task_completion_email.delay(serializer.instance.id, is_done)

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)

    def get_serializer_class(self):  # noqa
        if self.action == 'create':
            return TaskCreateSerializer
        if self.action == 'list':
            return TaskListSerializer
        elif self.action in ['update', 'partial_update']:
            return TaskUpdateSerializer
        return self.serializer_class

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return JsonResponse({'error': NO_TASK}, status=404)
        return super().handle_exception(exc)


class TaskExecuteView(UpdateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskIsDoneSerializer
    permission_classes = [permissions.IsAuthenticated, IsOwner]
    lookup_field = 'id'
    http_method_names = ['put']

    def perform_update(self, serializer):
        serializer.save(is_done=self.request.data.get('is_done'))
        send_task_completion_email.delay(serializer.instance.id, serializer.instance.is_done)

    def handle_exception(self, exc):
        if isinstance(exc, Http404):
            return JsonResponse({'error': NO_TASK}, status=404)
        return super().handle_exception(exc)
