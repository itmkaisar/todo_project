from rest_framework.exceptions import NotAuthenticated
from rest_framework.response import Response
from rest_framework.views import exception_handler

from utils import messages


def get_full_details(detail, response):  # noqa
    if isinstance(detail, list):
        for item in detail:
            return get_full_details(item, response)

    elif isinstance(detail, dict):
        return {key: get_full_details(value, response) for key, value in detail.items()}

    if detail in ['Token is invalid or expired', 'token_not_valid']:
        error_payload = {
            'status_code': 401,
            'message': messages.TEXT_UNAUTHORIZED,
        }
        return error_payload

    error_payload = {
        'status_code': 0,
        'message': detail,
    }
    status_code = response.status_code
    error_payload['status_code'] = status_code
    return error_payload


def custom_exception_handler(exc, context):  # noqa
    if type(exc) in (ValueError,):
        return Response({'message': str(exc), 'status': 401}, status=401)
    if isinstance(exc, NotAuthenticated):
        return Response({'message': messages.TEXT_ERROR_UNAUTHORIZED, 'status': 401}, status=401)

    response = exception_handler(exc, context)

    if response is not None:
        response.data = get_full_details(detail=exc.detail, response=response)

    return response
